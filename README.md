# dyns

You can try at https://dyns.mephi.ru/

Screenshots
ODE solving:
![image](https://user-images.githubusercontent.com/69370969/140618210-2eeb08b9-539b-4d09-9f89-e1977c24e979.png)

PDE sloving:
![image](https://user-images.githubusercontent.com/69370969/140618520-ab16711a-9425-49b0-add4-ad7f12d9e4c0.png)

ODE of 2nd order solving:
![image](https://user-images.githubusercontent.com/69370969/140618418-89f58777-22f0-4cf7-88fd-c1bfb558d560.png)

The program code was implemented as part of the course "Project Practice" during 2020-2021 by students Raul Nurievich Karachurin and Stanislav Arkadievich Ladygin of the Institute of Intelligent Cyber Systems MEPhI

Программный код реализован в рамках курса «Проектная практика» в течение 2020-2021 годов студентами Карачуриным Раулем Нуриевичем и Ладыгиным Станиславом Аркадьевичем Института интеллектуальных кибрнетических систем НИЯУ МИФИ
